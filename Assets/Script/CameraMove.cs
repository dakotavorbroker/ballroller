﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMove : MonoBehaviour {
    GameObject playerObj;
    Vector3 cameraOffset;

    // Use this for initialization
    void Start () {
        playerObj = GameObject.Find("Player");
        cameraOffset = new Vector3(1, 7, 0);
	}
	
	// Update is called once per frame
	void Update () {
        transform.position = playerObj.transform.position + cameraOffset;
	}
}
