﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {
    public int points = 0;

    private Rigidbody rb;
    private bool ismovingforward = false;
    private bool ismovingright = false;
    private float canJump = 0f;

    public int jumpHeight;
    float jumpSpeed;
    Vector3 velocity;

    [SerializeField]
    float speed = 4f;
	
	void Start () {

        rb = this.GetComponent<Rigidbody>();
        jumpSpeed = Mathf.Sqrt(-2 * Physics.gravity.y * jumpHeight) + 0.1f;

    }
	
	// Update is called once per frame
	void Update () {
        
        if (Input.GetKeyDown("w"))
        {
            rb.velocity = new Vector3(0f,0f,4f);
            ///ChangeDirection();
            //ChangebolleanForward();
        }
        
        if (Input.GetKeyDown("d"))
        {
            rb.velocity = new Vector3(4f, 0f, 0f);
            //ChangeDirectionRight();
            //ChangebolleanRight();
        }
        if (Input.GetKeyDown("a"))
        {
            rb.velocity = new Vector3(-4f, 0f, 0f);
            //ChangeDirectionRight();
            //ChangebolleanRight();
        }
        if (Input.GetKeyDown("space") && Time.time > canJump)
        {

            //velocity = rb.velocity;
            //velocity.y = jumpSpeed;
            rb.velocity = new Vector3(0f, 4f, 0f);
            canJump = Time.time + 1.5f;    // whatever time a jump takes
        }


    }


    private void ChangebolleanForward()
    {
        ismovingforward = ismovingforward;
    }
    private void ChangebolleanRight()
    {
        ismovingright = ismovingright;
    }



    private void ChangeDirectionRight()
    {
        /*
        if (ismovingforward)
        {
            rb.velocity = new Vector3(speed, 0f, 0f);
        }
        else
        {
            rb.velocity = new Vector3(0f, 0f, speed);
        }
        */
        if (ismovingright)
        {
            rb.velocity = new Vector3(0, -1, 0);
        }
        /*
        else
        {
            rb.velocity = new Vector3(0f, 0f, speed);
        }
        */
    }
    private void OnGUI()
    {
        GUI.Label(new Rect(75, 50, 100, 20),"Score: " + points);
    }
}
