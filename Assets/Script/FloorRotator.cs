﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorRotator : MonoBehaviour {
    //public float rotateSpeed = 10f;
    //public float min = 2f;
    //public float max = 3f;

   Vector3 pointA = new Vector3(-5, -33, 43);
    Vector3 pointB = new Vector3(4, -33, 43);

    //void Start () {
    //min = transform.position.x;
    // max = transform.position.x + 3;

    //}
    // Update is called once per frame
    void Update()
    {


        //transform.Rotate(Vector3.right, rotateSpeed * Time.deltaTime);
        //transform.position = new Vector3(Mathf.PingPong(Time.time * 2, max - min) + min, transform.position.y, transform.position.x);
       transform.position = Vector3.Lerp(pointA, pointB, Mathf.PingPong(Time.time/4, 1));
    }

}
